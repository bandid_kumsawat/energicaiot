var mongoose = require('mongoose');
// var uniqueValidator = require('mongoose-unique-validator');

var ProvinceSchema = new mongoose.Schema({
    pid:String,
    name:String
},{timestamps: true});

mongoose.model('Province', ProvinceSchema);