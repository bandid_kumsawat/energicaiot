var mongoose = require('mongoose');
// var uniqueValidator = require('mongoose-unique-validator');

var StationSchema = new mongoose.Schema({
    station_id:String,
    name:String,
    province_id:String,
    province:String,
    img:String,
    gf_url:String,
    dev_list:Array

},{timestamps: true});

mongoose.model('Station', StationSchema);