var mongoose = require('mongoose');

var SensorLogSchema = new mongoose.Schema({
    device_id:String,
    data:Array,
    timestamp:Date

},{timestamps: true},{ collection : 'sensorlogs' });


mongoose.model('SensorLog', SensorLogSchema);