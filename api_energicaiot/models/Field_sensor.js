var mongoose = require('mongoose');
// var uniqueValidator = require('mongoose-unique-validator');

var Field_sensorSchema = new mongoose.Schema({
    field_id:String,
    sensor_name:String,
    f_name:String,
    unit:String,
    tag:String,
    type:String
});

mongoose.model('Field_sensor', Field_sensorSchema);