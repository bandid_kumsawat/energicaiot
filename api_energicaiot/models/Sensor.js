var mongoose = require('mongoose');

var SensorSchema = new mongoose.Schema({
    sensor_id:{type: String,uppercase:true, unique: true,index: true},
    device_id:String,
    sensor_desc:String,
    sensor_name:String,
    field_device_id:String,
    tag:String,
    val:Number,
    unit:String

},{timestamps: true});

mongoose.model('Sensor', SensorSchema);