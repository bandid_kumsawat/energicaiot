var mongoose = require('mongoose');
// var uniqueValidator = require('mongoose-unique-validator');

var DeviceSchema = new mongoose.Schema({

    device_id:{type: String, lowercase: true, unique: true,index: true},
    type:{type: String, lowercase: true},
    device_name:String,
    station_id:String,
    station_name:String,
    province_id:String,
    province_name:String,
    user_monitor:Array,
    img:String,
    sensor:[{type:Object}],
    sw:[{type:Object}]
    
},{timestamps: true});

mongoose.model('Device', DeviceSchema);