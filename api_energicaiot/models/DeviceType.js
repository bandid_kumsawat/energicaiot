var mongoose = require('mongoose');
// var uniqueValidator = require('mongoose-unique-validator');

var typeSchema = new mongoose.Schema({
    id:String,
    type:String,
    text:String
},{collection: 'devicetype'});

mongoose.model('DeviceType', typeSchema);