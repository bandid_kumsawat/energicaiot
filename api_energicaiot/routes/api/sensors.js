var router = require('express').Router();
var mongoose = require('mongoose');
var Field_sensor = mongoose.model('Field_sensor');
var Sensor = mongoose.model('Sensor');
var Device = mongoose.model('Device')


router.get('/', function(req, res, next) {

    var qry = {};

    if(typeof req.query.sensor_id != "undefined"){
        var qid  = req.query.sensor_id;
       qry.sensor_id = qid;   
    }

    if(typeof req.query.device_id != "undefined"){
        var qid  = req.query.device_id;
       qry.device_id = qid;   
    }

    if(typeof req.query.tag != "undefined"){
        var tag_qry  = req.query.tag;
       qry.tag = tag_qry;   
    }
    
 
    Sensor.find(qry).then(function(ret){
        return res.json(ret);
    }).catch(next);
    
});

router.post('/',function(req,res,next){
    
    if (!req.body.sensor) { return res.sendStatus(401); }    

    if(!req.body.sensor.field_id){
        return res.status(422).json({field_id: "can't be blank"});
    }

    var fid  = req.body.sensor.field_id;
    var desc = req.body.sensor.sensor_desc;

    var qry = {field_id:fid};
    var qry2 = {device_id:"",field_device_id:fid}

    if(typeof req.body.sensor.device_id !== 'undefined' ){
        qry2.device_id = req.body.sensor.device_id;
    }

    Promise.all([
        Field_sensor.findOne(qry),
        Sensor.find(
            { field_device_id:fid}).sort({
            sensor_id: -1 
        }),
        Sensor.count(qry2)
    ]).then(function(result){

        // 
        var f_sensor = result[0]
        
        var id = "000001"

        if(result[1][0]){
            var serial = result[1][0].sensor_id
            console.log(serial)
            var num = parseInt(serial.substring(serial.length-6, serial.length));
            var str = "" + (num+1)
            var pad = "000000"
            id = pad.substring(0, pad.length - str.length) + str
        }
       
        console.log(f_sensor.f_name+id)
        
        // Add for sensor has deviceID
        var count = result[2];
        console.log(result[2])



        var sensor_new = new Sensor();

        sensor_new.sensor_id = f_sensor.f_name+id
        sensor_new.sensor_desc     = desc
        sensor_new.field_device_id = fid
        sensor_new.sensor_name = f_sensor.sensor_name
        sensor_new.val = 0
        sensor_new.unit = f_sensor.unit

        if(typeof req.body.sensor.device_id !== 'undefined' ){
            sensor_new.device_id = req.body.sensor.device_id;
            sensor_new.tag = f_sensor.tag + (count+1)
        }
        sensor_new.save().then(function(){
            return res.json(sensor_new);
        })

    }).catch(next);



});


router.get('/type',function(req,res,next){
    
    Field_sensor.find({}).then(function(ret){
        return res.json(ret);
        
    });
});


router.put('/',function(req,res,next){


    return res.json({msg:"TBA"});


});




module.exports = router;