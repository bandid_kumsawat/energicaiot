var router = require('express').Router();
var mongoose = require('mongoose');
var mqttClient = require('./mqtt_handler');


// Routes
router.post("/send-mqtt", function(req, res) {
    console.log(req.body.ss)
    try {
      var ss = req.body.ss
      var topic = "energica/"+ss.device_id+"/"+ss.tag
      mqttClient.sendMessage(topic,ss.val);
      res.status(200).json({"topic":topic,"val":ss.val});
      
    } catch (error) {
      console.log(error)  
    }
    
  });

module.exports = router;
