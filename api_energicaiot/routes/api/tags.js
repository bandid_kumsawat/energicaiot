var router = require('express').Router();
var mongoose = require('mongoose');
var Article = mongoose.model('Article');

// return a list of tags
router.get('/', function(req, res, next) {
  Article.find().distinct('tagList').then(function(tags){
    // return res.json({tags: tags});


    var ret_ener = {
      "total": 2,
      "per_page": 15,
      "current_page": 1,
      "last_page": 1,
      "next_page_url": null,
      "prev_page_url": null,
      "from": 1,
      "to": 1,
      "data": [
          {
              "project_id" : "en20190001",
              "project_name" : "ระบบน้ำร้อนอาคาร1",
              "station_id" : "1",
              "station_name":"Centara Grand Hotel Bangkok",
              "province_id" : "10",
              "user_monitor" : [ 
                  "user1", 
                  "admin", 
                  "user2"
              ],
              "province" : [ 
                  {
                      "pid" : "10",
                      "name" : "กรุงเทพมหานคร"
                  }
              ]
  
          }
      ]
  }



    return res.json(ret_ener);
  }).catch(next);
});

module.exports = router;
