var router = require('express').Router();
var mongoose = require('mongoose');
var underjs = require('underscore');
var Sensor = mongoose.model('Sensor');
var SensorLog = mongoose.model('SensorLog')
// var Device = mongoose.model('Device')

router.get('/', function(req, res, next){
    
    var qry = {};

    if(typeof req.query.device_id != "undefined"){
        var qid  = req.query.device_id;
        qry.device_id = qid

        SensorLog.find(qry).then(function(result){
            return res.json(result);
        }).catch(next);
    
    }else{
        return res.json({error:"no device_id"});
    }
    
});

router.post('/', function(req, res, next) {

    if (!req.body.device_id) { return res.status(422).json({error: "device_id can't be blank"}); }    

    if (!req.body.data) { return res.status(422).json({error: "no Data"}); }    

    

    Sensor.find({"device_id":req.body.device_id}).then(function(result){
        underjs.each(req.body.data,function(dd){
            console.log(dd.t);
            var ss_save = underjs.find(result, function(num){ return num.tag  === dd.t; });
            
            ss_save.val = dd.v;
            ss_save.timestamp = new Date();
            ss_save.save().then(function(err){
                console.log(ss_save.sensor_id)
            });
        });
    });


    var log_ss = new SensorLog();
    log_ss.device_id = req.body.device_id;
    log_ss.data = req.body.data;
    log_ss.timestamp = new Date();
    log_ss.save().then(function(){
        return res.json(log_ss);
    }).catch(next);

});


module.exports = router;