var router = require('express').Router();
var mongoose = require('mongoose');
var Station = mongoose.model('Station');
var Province = mongoose.model('Province');
var Device = mongoose.model('Device');
var Sensor = mongoose.model('Sensor');
var underjs = require('underscore');

var auth = require('../auth');
// return a list of tags
router.get('/', function(req, res, next) {

  Station.find({}).then(function(result){
    return res.json(result);
  }).catch(next);
});



router.get('/:st_id', function(req, res, next) {

  
  Device.aggregate([
    { "$match": { "station_id": req.params.st_id } },
    { $lookup:
      {
         from: "sensors",
         localField: "device_id",
         foreignField: "device_id",
         as: "sensor"
      }
    },
    { $lookup:
      {
         from: "io",
         localField: "device_id",
         foreignField: "device_id",
         as: "sw"
      }
    },
    { $lookup:
      {
         from: "analog",
         localField: "device_id",
         foreignField: "device_id",
         as: "an"
      }
    },
    {
      $sort:{"sensor.tag":-1}
    }
  ]).exec((err, result) => {
      if (err) throw err;

      underjs.each(result, function(ss){
        ss.sensor = underjs.sortBy(ss.sensor, 'tag'); 
        
        var sensor_last = underjs.max(ss.sensor, function(data){ return data.updatedAt; });
        ss.lasttime = sensor_last.updatedAt;
      });

    
      // result.sensor = underjs.sortBy(result.sensor, 'tag'); 
      // console.log(result);     
      return res.json(result)
  })
});



router.post('/', function(req, res, next) {

    if (!req.body.station.name) { return res.sendStatus(401); }    

  
    if(!req.body.station.province_id){
      return res.status(422).json({errors: {province_id: "can't be blank"}});
    }
    
    
    Province.findOne({"pid":req.body.station.province_id}).then(function(ret){
      if(ret){
       
        var station = new Station();

        /* **** OLD 
        Station.count({}, function(err, count){
          var p_id = count+1;
    
          station.station_id   = p_id.toString();
          station.name = req.body.station.name;
          station.province_id = ret.pid
          station.province = ret.name
           
          station.save().then(function(){
            return res.json({"count":p_id});
          }).catch(next);
        });
        */

       Station.find().sort({
        station_id: -1 
       }).then(function(result){

        console.log(result)

        var st = result
        
        var p_id = 1

          if(result.length > 0){
              var serial = result[0].station_id
              console.log(serial)
              var num = parseInt(serial);
              p_id = num+1
          }

          station.station_id   = p_id.toString();
          station.name = req.body.station.name;
          station.province_id = ret.pid
          station.province = ret.name
          station.img = req.body.station.img;
          station.img = req.body.station.gf_url;
           

          station.save().then(function(){
            return res.json({"count":p_id});
          }).catch(next);

       });
        
      }else {
         return res.status(422).json({errors: {province_id: "Not Found"}});
      }
    });

});


router.put('/', function(req, res, next){

  if(!req.body.station_id){
    return res.status(422).json({errors: {station_id: "can't be blank"}});
  }

  
  Promise.all([ 
    Station.findOne({"station_id":req.body.station_id}),
    Province.findOne({"pid":req.body.province_id})
  ]).then(function(result){
    var tmp_msg  = result[0];
    var province = result[1];

    // console.log(tmp_msg)
    // console.log(province);

    if (!result[0]){
      return res.status(422).json({errors: {station_id: "no ID"}});
    }

    if(typeof req.body.name !== 'undefined'){
      tmp_msg.name = req.body.name;
    }

    if(typeof req.body.img !== 'undefined'){
      tmp_msg.img = req.body.img;
    }

    if(typeof req.body.gf_url !== 'undefined'){
      tmp_msg.gf_url = req.body.gf_url;
    }

    if (province){
      tmp_msg.province    = province.name
      tmp_msg.province_id = province.pid      
    }

    tmp_msg.save().then(function(err){
      console.log(err)
      return res.json(tmp_msg);
    })   
  }).catch(next);



  
});

router.delete('/',function(req,res,next){

  if(typeof req.query.station_id === 'undefined'){
      return res.status(422).json({error:"no station_id"})
  }


  Station.findOneAndRemove({"station_id":req.query.station_id}, (err, ret) => {
    
    if (!ret) return res.status(500).send({ message: "No station ID"});

    const response = {
        message: "successfully deleted",
        id: req.query.station_id
    };
    return res.status(200).send(response);
  });

});

module.exports = router;
