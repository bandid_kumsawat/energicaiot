var router = require('express').Router();
var mongoose = require('mongoose');
var Article = mongoose.model('Article');

// return a list of tags
router.get('/', function(req, res, next) {
  Article.find().distinct('tagList').then(function(projects){
    // return res.json({tags: tags});


    var ret_ener = {
      "total": 2,
      "data": [
          {
              "project_id" : "en20190001",
              "project_name" : "ระบบน้ำร้อนอาคาร1",
              "station_id" : "1",
              "station_name":"Centara Grand Hotel Bangkok",
              "province_id" : "10",
              "user_monitor" : [ 
                  "user1",
              ],
              "province_name":"กรุงเทพมหานคร",
              "province" : [ 
                  {
                      "pid" : "10",
                      "name" : "กรุงเทพมหานคร"
                  }
              ]
  
          },
          {
            "project_id" : "en20190002",
            "project_name" : "ระบบน้ำร้อนอาคาร2",
            "station_id" : "1",
            "station_name":"Centara Grand Hotel Bangkok",
            "province_id" : "10",
            "user_monitor" : [ 
                "user1", 
                "admin", 
                "user2"
            ],
            "province_name":"กรุงเทพมหานคร",
            "province" : [ 
                {
                    "pid" : "10",
                    "name" : "กรุงเทพมหานคร"
                }
            ]
        },
        {
            "project_id" : "en20190003",
            "project_name" : "ระบบน้ำร้อนตึกเก่า",
            "station_id" : "1",
            "station_name":"Centara Grand Hotel Krabi",
            "province_id" : "10",
            "user_monitor" : [ 
                "user1", 
                "admin", 
                "user2"
            ],
            "province_name":"กระบี่",
            "province" : [ 
                {
                    "pid" : "10",
                    "name" : "กระบี่"
                }
            ]

        }
      ]
  }



    return res.json(ret_ener);
  }).catch(next);
});

module.exports = router;
