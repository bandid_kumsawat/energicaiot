var router = require('express').Router();
var mongoose = require('mongoose');
var Device = mongoose.model('Device');
var Sensor = mongoose.model('Sensor');
var Station= mongoose.model('Station');
var DeviceType   = mongoose.model('DeviceType') 

router.get('/', function(req, res, next) {

    var qry = {};

    if(typeof req.query.station_id != "undefined"){
        var qid  = req.query.station_id;
        qry.station_id = qid;
    }

    if(typeof req.query.device_id != "undefined"){
        var qid  = req.query.device_id;
    
        qry.device_id = qid;

        Promise.all([
            Device.find(qry),        
            Sensor.find(qry)
        ]).then(function(result){
            var tmp_msg  = result[0][0];
            var sensor_list = result[1];
    
            tmp_msg.sensor = sensor_list;
            return res.json(tmp_msg)        
    
        }).catch(next);
    
    
    
    }else{
        Device.find(qry).then(function(ret){
            return res.json(ret);
        }).catch(next);
    }
 


});


router.get('/type', function(req, res, next) {

    DeviceType.find({}).then(function(ret){
        return res.json(ret);
    }).catch.next();

});



router.get('/:device', function(req, res, next) {
    console.log(req.params)
    
    var qry = {};
    var return_msg = {};
    var i=0;
    qry.device_id =req.params.device;
 
    Promise.all([
        Device.find(qry),        
        Sensor.find(qry)
    ]).then(function(result){
       
        var tmp_msg  = result[0][0];
        var sensor_list = result[1];

        tmp_msg.sensor = sensor_list;
   
        return res.json(tmp_msg)        

    }).catch(next);



    // return res.json({"id":"asdfzxcv"})
});



router.post('/', function(req, res, next) {

    if (!req.body.device) { return res.sendStatus(401); }    

    if(!req.body.device.station_id){
        return res.status(422).json({station_id: "can't be blank"});
    }

    if(!req.body.device.type){
        return res.status(422).json({type: "can't be blank"});
    }

    var type_msg = req.body.device.type;

    var text="";

    switch(type_msg) {
        case "boiler":
          text = "bol";
          break;
        case "heatpump":
          text = "heat";
          break;
        case"vsd":
          text = "vsd";
          break;
        case"chiler":
          text = "chl";
        case"AHU":
          text = "ahu";
          break;
        default:
          return res.status(422).json({type: "No type for Device"});
    }
   


   Device.find({type:type_msg}).sort({
    device_id: -1 
   }).then(function(result,err){
   

    var id_srt = result[0].device_id

    id_srt = id_srt.substring(id_srt.length-6 ,id_srt.length)


    var str = "" + (parseInt(id_srt) +1)
    var pad = "000000"
    var id = pad.substring(0, pad.length - str.length) + str

    console.log(id_srt);
    console.log(text+"-"+id);

    var device_new = new Device();

    device_new.device_id = text+"-"+id;       
    device_new.device_name = req.body.device.device_name;
    device_new.type = type_msg;
    device_new.station_id = req.body.device.station_id;
    device_new.station_name = req.body.device.station_name;
    device_new.province_id = req.body.device.province_id;
    device_new.province_name = req.body.device.province_name;
    device_new.user_monitor = req.body.device.user_monitor;
    device_new.img = req.body.device.img;

      
    device_new.save().then(function(){
        return res.json(device_new);
    }).catch(next);

   });


});

router.put("/",function(req, res, next){
    if(!req.body.device_id){
        return res.status(422).json({errors: {device_id: "can't be blank"}});
    }

    Promise.all([ 
        Device.findOne({"device_id":req.body.device_id}),
        Station.findOne({"station_id":req.body.station_id})
      ]).then(function(result){

        var tmp_msg  = result[0];
        var station_ret = result[1];

        if (!result[0]){
            return res.status(422).json({errors: {device_id: "no ID"}});
        }

        if(typeof req.body.device_name !== 'undefined'){
            tmp_msg.device_name = req.body.device_name;
        }

        if(typeof req.body.img !== 'undefined'){
            tmp_msg.img = req.body.img;
        }

        
        if (station_ret){
            tmp_msg.province_name= station_ret.province
            tmp_msg.province_id  = station_ret.province_id    
            tmp_msg.station_name = station_ret.name
            tmp_msg.station_id   = station_ret.station_id      
        }
        tmp_msg.save().then(function(err){
            console.log(err)
            return res.json(tmp_msg);
        })   
      
    }).catch(next);
});


router.delete('/',function(req,res,next){

    if(typeof req.query.device_id === 'undefined'){
        return res.status(422).json({error:"no device_id"})
    }
  
  
    Device.findOneAndRemove({"device_id":req.query.device_id}, (err, ret) => {
      
      if (!ret) return res.status(500).send({ message: "No device ID"});
  
      const response = {
          message: "successfully deleted",
          id: req.query.device_id
      };
      return res.status(200).send(response);
    });
  
  });

module.exports = router;