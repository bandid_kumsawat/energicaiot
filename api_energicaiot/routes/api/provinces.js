var router = require('express').Router();
var mongoose = require('mongoose');
var Province = mongoose.model('Province');


router.get('/', function(req, res, next) {
    Province.find({}).then(function(result){
      return res.json(result);
    }).catch(next);
});

module.exports = router;