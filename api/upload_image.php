<?php
    $target_dir = "./../images/";
    $target_file = $target_dir . $_POST['device_id'] . '.png'; //basename($_FILES["upload_img"]["name"]);
    $uploadOk = 1;
    $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
    // Check if image file is a actual image or fake image
    if(isset($_POST["submit"])) {
        $check = getimagesize($_FILES["upload_img"]["tmp_name"]);
        if($check !== false) {
            echo "<center style = 'padding-top: 40px;'><h1>File is an image - " . $check["mime"] . ".</h1></center>";
            $uploadOk = 1;
        } else {
            echo "<center style = 'padding-top: 40px;'><h1>File is not an image.</h1></center>";
            $uploadOk = 0;
        }
    }
    // Check if file already exists
    // if (file_exists($target_file)) {
    //     echo "<center style = 'padding-top: 40px;'><h1>Sorry, file already exists.</h1></center>";
    //     $uploadOk = 0;
    // }
    // Check file size
    if ($_FILES["upload_img"]["size"] > 5000000) {
        echo "<center style = 'padding-top: 40px;'><h1>Sorry, your file is too large.</h1></center>";
        $uploadOk = 0;
    }
    // Allow certain file formats
    if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
    && $imageFileType != "gif" ) {
        echo "<center style = 'padding-top: 40px;'><h1>Sorry, only JPG, JPEG, PNG & GIF files are allowed.</h1></center>";
        $uploadOk = 0;
    }
    // Check if $uploadOk is set to 0 by an error
    if ($uploadOk == 0) {
        echo "<center style = 'padding-top: 40px;'><h1>Sorry, your file was not uploaded.</h1></center>";
    // if everything is ok, try to upload file
    } else {
        if (move_uploaded_file($_FILES["upload_img"]["tmp_name"], $target_file)) {
            echo "The file ". basename( $_FILES["upload_img"]["name"]). " has been uploaded.";
                            // change image at Api_Device 
                            $curl = curl_init();

                            curl_setopt_array($curl, array(
                            CURLOPT_PORT => "4000",
                            CURLOPT_URL => "http://www.energicaiot.com:4000/api/device",
                            CURLOPT_RETURNTRANSFER => true,
                            CURLOPT_ENCODING => "",
                            CURLOPT_MAXREDIRS => 10,
                            CURLOPT_TIMEOUT => 30,
                            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                            CURLOPT_CUSTOMREQUEST => "PUT",
                            CURLOPT_POSTFIELDS => "{ \n\t\"device_id\":\"" . $_POST['device_id'] . "\",\n    \"img\": \"" . $_POST['device_id'] . '.png' . "\",\n    \"station_id\": \"" . $_POST['station'] . "\"\n}",
                            CURLOPT_HTTPHEADER => array(
                                "Accept: */*",
                                "Cache-Control: no-cache",
                                "Connection: keep-alive",
                                "Content-Type: application/json",
                                "cache-control: no-cache"
                            ),
                            ));

                            $response = curl_exec($curl);
                            $err = curl_error($curl);

                            curl_close($curl);

                            if ($err) {
                                echo "<center style = 'padding-top: 40px;'><h1>cURL Error #:" . $err . "</h1></center>";
                            } else {
                                // echo $response;
                                header("Location: ./../index.php");
                            }

            // header("Location: ./../index.php");
        } else {
            echo "<center style = 'padding-top: 40px;'><h1>Sorry, there was an error uploading your file.</h1></center>";
        }
    }
?>