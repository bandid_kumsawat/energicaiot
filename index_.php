<?php 
  @session_start();
  if (isset($_SESSION['username'])){

  }else{
    header("location: login.php");
  }
  if (isset($_GET['station_id'])){
    $_SESSION['station_id'] = $_GET['station_id'];
  }
?>
<!-- 
ชลจันทร์ พัทยา บีช รีสอร์ท (Cholchan Pattaya Beach Resort)
โรงแรมบางกอกพาเลส (Bangkok Palace Hotel)
โรงแรมเซ็นทาราและคอนเวนชันเซ็นเตอร์ อุดรธานี (Centara Hotel & Convention Centre Udon Thani) 
-->
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Energica IoT</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="bower_components/jvectormap/jquery-jvectormap.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <link rel="stylesheet" type="text/css" href="engine0/style.css" />
  <!-- button toggle -->
  <link rel="stylesheet" type="text/css" href="button-toggle/css/bootstrap2-toggle.min.css" />
  <!-- <link rel="stylesheet" type="text/css" href="button-toggle/css/bootstrap2-toggle.css  " /> -->

  <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
  <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
  
  <!-- google font -->
  <link href="https://fonts.googleapis.com/css?family=Kanit|Noto+Sans&display=swap" rel="stylesheet">
</head> 
<style>
  * {
    font-family: 'Kanit', sans-serif;
    font-style: Thin;
  }
  body {
    font-family: 'Kanit', sans-serif;
    /* font-size: 16px; */
    background-image: url('./data0/images/Udon09.png');
    background-size: 100%;
    background-repeat: no-repeat;
    /* padding-top: 8%; */
  }
  #map-and-img-1 {
    padding: 0px 10px 0px 0px;
  }
  #map-and-img-2 {
    padding: 0px 0px 0px 10px;
  }
  @media only screen and (max-width: 960px) {
    /* For mobile phones: */
    #map-and-img-1 {
      padding: 0px 0px 10px 0px;
    }
    #map-and-img-2 {
      padding: 10px 0px 0px 0px;
    }
  }


  .switch {
    position: relative;
    display: inline-block;
    width: 60px;
    height: 34px;
  }

  .switch input { 
    opacity: 0;
    width: 0;
    height: 0;
  }

  .slider {
    position: absolute;
    cursor: pointer;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background-color: #ccc;
    -webkit-transition: .4s;
    transition: .4s;
  }

  .slider:before {
    position: absolute;
    content: "";
    height: 26px;
    width: 26px;
    left: 4px;
    bottom: 4px;
    background-color: white;
    -webkit-transition: .4s;
    transition: .4s;
  }

  input:checked + .slider {
    background-color: #a31640;
  }

  input:focus + .slider {
    box-shadow: 0 0 1px #2196F3;
  }

  input:checked + .slider:before {
    -webkit-transform: translateX(26px);
    -ms-transform: translateX(26px);
    transform: translateX(26px);
  }

  .slider.round {
    border-radius: 34px;
  }

  .slider.round:before {
    border-radius: 50%;
  }


/* Device status active and inactive show color  */
  .active-color {
    color: #2f9605;
    -webkit-animation-name: acive_co; 
    -webkit-animation-duration: 0.5s; 
    animation-name: acive_co;
    animation-duration: 0.5s;
  }

  .inactive-color {
    color: #8d8b8c;
    -webkit-animation-name: inacive_co; 
    -webkit-animation-duration: 0.5s; 
    animation-name: inacive_co;
    animation-duration: 0.5s;
  }
  .box-active {
    color: #000000;
    padding-top : 3px;
    padding-left : 10px;
    padding-right : 10px;
    border-radius : 4px;
    opacity: 0.9;
    background-color: #2f9605;
    -webkit-animation-name: box_atcive_co; 
    -webkit-animation-duration: 0.5s; 
    animation-name: box_atcive_co;
    animation-duration: 0.5s;
  }
  @-webkit-keyframes box_atcive_co {
    from {background-color: #2f9605;}
    to {background-color: #50ac17;}
  }

  @keyframes box_atcive_co {
    from {background-color: #2f9605;}
    to {background-color: #50ac17;}
  }
  .box-inactive {
    color: #000000;
    padding-top : 3px;
    padding-left : 10px;
    padding-right : 10px;
    border-radius : 4px;
    opacity: 0.9;
    background-color: #8d8b8c;
    -webkit-animation-name: box_inatcive_co; 
    -webkit-animation-duration: 0.5s; 
    animation-name: box_inatcive_co;
    animation-duration: 0.5s;
  }
    @-webkit-keyframes box_inatcive_co {
    from {background-color: #aca9aa;}
    to {background-color: #8d8b8c;}
  }

  @keyframes box_inatcive_co {
    from {background-color: #aca9aa;}
    to {background-color: #8d8b8c;}
  }
  @-webkit-keyframes acive_co {
    from {color: #2f9605;}
    to {color: #50ac17;}
  }

  @keyframes acive_co {
    from {color: #2f9605;}
    to {color: #50ac17;}
  }
  @-webkit-keyframes inacive_co {
    from {color: #aca9aa;}
    to {color: #8d8b8c;}
  }

  @keyframes inacive_co {
    from {color: #aca9aa;}
    to {color: #8d8b8c;}
  }
 /* end  */
  </style>
<body class="sidebar-mini wysihtml5-supported fixed skin-blue">
<div style="font-weight:400;">
  <div class="wrapper">

    <header class="main-header">
      <!-- Logo -->
      <a href="#" class="logo" style = "background-color:#a31640;">
        <span class="logo-mini"><b>IoT</b></span>
        <span class="logo-lg"><b>Energica IoT</b></span>
        <!-- <img src = "./images/logo.png"/ width="120px" height="50px" style = "padding-bottom: 10px;"> -->
      </a>
      <nav class="navbar navbar-static-top" style = "background-color:#a31640;opacity: 10;">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
          <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">


            <?php if ($_SESSION['type'] == "admin"){ ?>
              <li class="dropdown notifications-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-building"></i>
                  <span class="label label-warning">3</span>
                  <span class="hidden-xs">เลือกโรงแรม</span>
                </a>
                <ul class="dropdown-menu" >
                  <li class="header">เลือกโรงแรม</li>
                  <li>
                    <!-- inner menu: contains the actual data -->
                    <ul class="menu">
                      <li>
                        <a href="index.php?station_id=1">
                          <i class="fa  fa-circle-o" style = "color:#ec4174;"></i> โรงแรมเซ็นทาราและคอนเวนชันเซ็นเตอร์ อุดรธานี
                        </a>
                      </li>
                      <li>
                        <a href="index.php?station_id=2">
                          <i class="fa  fa-circle-o" style = "color:#9c294b;"></i> โรงแรมบางกอกพาเลส
                        </a>
                      </li>
                      <li>
                        <a href="index.php?station_id=3" onclick = "">
                          <i class="fa  fa-circle-o" style = "color:#6a122d;"></i> โรงแรมชลจันทร์ พัทยา บีช รีสอร์ท
                        </a>
                      </li>
                      <li>
                        <a href="index.php?station_id=4" onclick = "">
                          <i class="fa  fa-circle-o" style = "color:#6a122d;"></i> บริษัท เหรียญไทย อินเตอร์พลาส สาขาบางพลี
                        </a>
                      </li>
                      <li>
                        <a href="index.php?station_id=4" onclick = "">
                          <i class="fa  fa-circle-o" style = "color:#6a122d;"></i> บริษัท เหรียญไทย อินเตอร์พลาส จำกัด สาขา เวลโกรว์
                        </a>
                      </li>
                    </ul>
                  </li>
                </ul>
              </li>
            <?php }?>


            <li class="dropdown user user-menu">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <?php
                  if ($_SESSION['type'] == "admin"){
                ?>
                  <img src="images/admin.png " class="user-image" alt="User Image">
                <?php    
                  }else{
                ?>
                  <img src="images/user<?php echo $_SESSION['station_id'];?>.jpg" class="user-image" alt="User Image">
                <?php
                  }
                ?>
                <span class="hidden-xs"><?php echo $_SESSION['username'];?> [<?php echo $_SESSION['type'];?>]</span>
              </a>
              <ul class="dropdown-menu">
                <li class="user-header" style = "background-color:rgb(108, 108, 108);">

                  <?php
                    if ($_SESSION['type'] == "admin"){
                  ?>
                    <img src="images/admin.png" class="img-circle" alt="User Image">
                  <?php    
                    }else{
                  ?>
                    <img src="images/user<?php echo $_SESSION['station_id'];?>.jpg" class="img-circle" alt="User Image">
                  <?php
                    }
                  ?>

                  <p>
                    <i class="fa fa-user"></i> <?php echo $_SESSION['username'];?> [<?php echo $_SESSION['type'];?>]
                    <small>[<?php echo $_SESSION['type'];?>]</small>
                  </p>
                </li>
                <li class="user-footer">
                  <div class="pull-left">
                    <!-- <a href="#" class="btn btn-default btn-flat">Profile</a> -->
                  </div>
                  <div class="pull-right">
                    <a href="login.php" class="btn btn-default btn-flat"><i class="fa fa-sign-out"></i> Sign out</a>
                  </div>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </nav>
    </header>

    <aside class="main-sidebar">
      <section class="sidebar" >
        <ul class="sidebar-menu" data-widget="tree">
          <li class="header">MENU</li>
          <li class="active treeview">
            <a href="index.html">
              <i class="fa fa-dashboard"></i> <span style = "font-size:16px;"><strong>Dashboard</strong></span>
            </a>
          </li>
          <li class="treeview">
            <a href="#">
              <i class="fa fa-bar-chart"></i>
              <span style = "font-size:16px;"><strong>Statictics</strong></span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="chart.php" style = "font-size:16px;"><i class="fa fa-circle-o"></i> Chart</a></li>
            </ul>
          </li>
          <!-- <li class="treeview">
            <a href="#">
              <i class="fa fa-tv"></i>
              <span style = "font-size:16px;"><strong>Project</strong></span>
            </a>
          </li> -->
        </ul>
      </section>
      <!-- /.sidebar -->
    </aside>

    <div class="content-wrapper">


    <section class="content-header col-sm-12">
      
      <div class='box box-success'>
        <div class='box-header'>
        <i class="fa fa-star"></i>
          <h1 class='box-title' style = " font-size: 1.8em;padding-right:2%;">
            <!-- <strong>โรงแรมเซ็นทาราและคอนเวนชันเซ็นเตอร์ อุดรธานี (Centara Hotel & Convention Centre Udon Thani)</strong> -->
            <strong id = 'hotel_name'></strong>
          </h1>
        </div>
      </div>
    </section>


    <section class="content-header">
      <!-- <div id="wowslider-container0">
      <div class="ws_images">
        <ul>
          <li><img src="data0/images/Udon01.png" alt="Centara Hotel & Convention Centre Udon Thani" title="Centara Hotel & Convention Centre Udon Thani" id="wows0_0"/></li>
          <li><img src="data0/images/Udon02.png" alt="3e1e1815_z" title="3e1e1815_z" id="wows0_1"/></li>
          <li><img src="data0/images/Udon03.png" alt="bootstrap carousel" title="44921_15061113110029258650" id="wows0_2"/></a></li>
          <li><img src="data0/images/Udon04.png" alt="20111226_3_1324896695_680994" title="20111226_3_1324896695_680994" id="wows0_3"/></li>
        </ul>
      </div>
      </div> -->
      <!-- 
        ชลจันทร์ พัทยา บีช รีสอร์ท (Cholchan Pattaya Beach Resort)   location: 12.986083, 100.919444
        โรงแรมบางกอกพาเลส (Bangkok Palace Hotel)   location: 13.753945, 100.547264
        โรงแรมเซ็นทาราและคอนเวนชันเซ็นเตอร์ อุดรธานี (Centara Hotel & Convention Centre Udon Thani)  location : 17.407080, 102.800444
      -->
      <div class='container-fluid'>
        <div class='row'>
          <div class='col-md-8' id  = 'map-and-img-1'>
            <img id = "hotel_name" src="data0/images/<?Php echo $_SESSION['station_id'];?>.png" alt="bootstrap carousel" title="44921_15061113110029258650" width='100%' height="140em;"/>
          </div>
          <div class='col-md-4' id = 'map-and-img-2'>
            <!-- <div id="map" style="height: 10em;"></div> -->
            <div style="height: 10em;">
              <img src = "./images/map/<?php echo $_SESSION['station_id']; ?>.png" width="100%" height="100%"/>
            </div>
          </div>
        </div>
      </div>
        <!-- <img src="data0/images/Udon03.png" alt="bootstrap carousel" title="44921_15061113110029258650" width='100%'/>
        <div id="map" style="height: 350px"></div> -->
    </section>

        <section class="content-header">
          <h1>
            <small></small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
          </ol>
        </section>
        <section class="content">
          <div class="row">
            <section class="col-lg-12" id = 'section_box' >

            </section>
          </div>
        </section>
      </div>
    <footer class="main-footer">
      <strong>DEAWARE TEAM</strong>
      <div class="pull-right hidden-xs">
        <strong>Energica IoT</strong>
      </div>
    </footer>
    <div class="control-sidebar-bg"></div>
  </div>
</div>


<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="bower_components/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Sparkline -->
<script src="bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="bower_components/moment/min/moment.min.js"></script>
<script src="bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<!-- <script type="text/javascript" src="engine0/wowslider.js"></script>
<script type="text/javascript" src="engine0/script.js"></script> -->
<!-- button toggle -->
<script type="text/javascript" src="button-toggle/js/bootstrap2-toggle.min.js"></script>
<!-- <script type="text/javascript" src="button-toggle/js/bootstrap2-toggle.js"></script> -->
<!-- Google Maps -->
<!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA60weYi_4Yrt6C6GEiiiUegYnPBfWXaBk&callback=initMap" async defer></script> -->
<!-- underscore javascript -->
<script src="dist/js/underscore-min.js"></script>
<!-- moment datetime -->
<script src = "dist/js/moment.js"></script>



<script type="text/javascript" src="dist/js/underscore-min.js"></script>
<script language="JavaScript" >
  var time = 6;
  var unit_
  var map,table_s,marker;
  var obj;
  var user_type = "<?php echo $_SESSION['type'];?>";
  $( document ).ready(function() {
    // api_station_param();
    try {
      // api_station_param();
      first_load();
      // $("#vOut1").bootstrapToggle('disable');
    } catch (e) {
      location.reload();
    }
    setInterval(function(){ api_station_param();  }, time*1000);
    // $('#vOut1').bootstrapToggle({on: 'Enabled',off: 'Disabled'});
    // $('#chb1').change(function() {
    //   console.log($(this).prop('checked'));
    // })
  });
function initMap(location) {
  map = new google.maps.Map(document.getElementById('map'), {
    center: location,
    zoom: 16,
    mapTypeId:  google.maps.MapTypeId.HYBRID 
    // google.maps.MapTypeId.SATELLITE
  });

        // ชลจันทร์ พัทยา บีช รีสอร์ท (Cholchan Pattaya Beach Resort)   location: 12.986083, 100.919444
        // โรงแรมบางกอกพาเลส (Bangkok Palace Hotel)   location: 13.753945, 100.547264
        // โรงแรมเซ็นทาราและคอนเวนชันเซ็นเตอร์ อุดรธานี (Centara Hotel & Convention Centre Udon Thani)  location : 17.407080, 102.800444

  var myLatLng = location;

  var image = {				
    url:'./images/mark.png',
  };

  var content_in_map = [
    '<strong>โรงแรมเซ็นทาราและคอนเวนชันเซ็นเตอร์ อุดรธานี</strong>',
    '<strong>โรงแรมบางกอกพาเลส</strong>',
    '<strong>ชลจันทร์ พัทยา บีช รีสอร์ท</strong>'
  ];
  bounds = new google.maps.LatLngBounds();

  marker = new google.maps.Marker({
    position: myLatLng,
    map: map,
    title: "ที่ตั้งโรงแรม",
    icon: image,
  });

  bounds.extend(marker.position);

  info = new google.maps.InfoWindow({
    content: content_in_map[<?php echo $_SESSION['station_id']; ?> - 1]
  });
  
  // markers.push(marker);
  
  google.maps.event.addListener(marker, 'click', function() {
    info.open(map, marker);
  });
}

function api_station_param(){
  var settings = {
    "async": true,
    "crossDomain": true,
    "url": "./api/param_station.php?station_id=<?php echo $_SESSION['station_id'];?>",
    "method": "GET",
    "headers": {
      "cache-control": "no-cache",
    }
  };

  $.ajax(settings).done(function (response) {
    obj = JSON.parse(response);
    console.log(obj);
    
    // var date = new Date(obj[0].updatedAt);
    // console.log(date);

    // var a = moment("2019-07-01T09:49:01.476Zs",'YYYY-MM-DD HH:mm:ss');
    // var b = moment(moment().format('YYYY-MM-DD HH:mm:ss'),'YYYY-MM-DD HH:mm:ss');
    // var sum = b.diff(a, "hours");
    // console.log(a);
    // console.log(b);
    // console.log(sum);
    // // <i class='fa fa-circle active-color'></i>
    // console.log("#" + obj[0].device_id)
    // obj = _.sortBy(obj,'_id');
    // $("#" + obj[0].device_id).html("");
    // $('#' + obj[0].device_id).append("<i class='fa fa-circle active-color'></i>");
    // // var content_out = "";
    
    for (var i = 0;i < obj.length;i++){
      // var date = new Date(obj[i].lasttime);
      // var a = moment(date,'YYYY-MM-DD HH:mm:ss');
      // var b = moment(moment().format('YYYY-MM-DD HH:mm:ss'),'YYYY-MM-DD HH:mm:ss');
      // var sum = b.diff(a, "hours");
      // var t = moment(obj[i].lasttime).format("DD MMMM YYYY, HH:MM:SS");
      var date = moment(obj[i].lasttime,'YYYY-MM-DD HH:mm:ss').add(7,"hours");
      var a = moment(date,'YYYY-MM-DD HH:mm:ss');
      var b = moment(moment().format('YYYY-MM-DD HH:mm:ss'),'YYYY-MM-DD HH:mm:ss');
      var sum = b.diff(a, "hours");
      if (sum < 1){
        $("#" + obj[i].device_id).html("");
        $('#' + obj[i].device_id).append("<i class='fa fa-circle active-color'></i>");
        $("#date" + obj[i].device_id).html("");
        $("#date" + obj[i].device_id).append("<span class='box-active'>last active : "+date.format('YYYY-MM-DD HH:mm:ss')+"</span>");
      }else{
        $("#" + obj[i].device_id).html("");
        $('#' + obj[i].device_id).append("<i class='fa fa-circle inactive-color'></i>");
        $("#date" + obj[i].device_id).html("");
        $("#date" + obj[i].device_id).append("<span class='box-inactive'>last active : "+date.format('YYYY-MM-DD HH:mm:ss')+"</span>");
      }
      var content_out = "";
      for (var k = 0;k < obj[i].sensor.length;k++){
        content_out += "<tr style = 'font-size:0.85em;'>";  
          content_out += "<td <?php if ($_SESSION['type'] == "user"){echo "hidden";} ?>>"+obj[i].sensor[k].tag+"</td>";  
          content_out += "<td <?php if ($_SESSION['type'] == "user"){echo "hidden";} ?>>"+obj[i].sensor[k].sensor_name+"</td>"; 
          content_out += "<td>"+obj[i].sensor[k].sensor_desc+"</td>"; 


                      // change value and unit
          var unti_ = ""
          var value_ = 0
          if (obj[i].sensor[k].unit == "WH"){
            if (obj[i].sensor[k].val >= 1000000){
              value_ = (obj[i].sensor[k].val / 1000000).toFixed(3)
              unti_ =  "M"
              console.log(value_)
              console.log(unit_)
            }else if (obj[i].sensor[k].val >= 10000){
              value_ = obj[i].sensor[k].val / 1000
              unti_ = "K"
            }
          }else{
            value_ = obj[i].sensor[k].val
            unit_ = ""
          }

          content_out += "<td id = '' style = 'font-size: 1.6em;color: #c5306a;'><center>"+value_+"</center></td>"; 
          content_out += "<td id = '' ><center>" + unti_ + obj[i].sensor[k].unit+"</center></td>"; 
          content_out += "<td>";
          content_out += "</td>"; 
        content_out += "</tr>";
      }
      // console.log("#body_t"+obj[i].device_id);
      $("#body_t"+obj[i].device_id).html("");
      $("#body_t"+obj[i].device_id).append(content_out);
    }
 });
} 

function first_load(){
  var settings = {
    "async": true,
    "crossDomain": true,
    "url": "./api/param_station.php?station_id=<?php echo $_SESSION['station_id'];?>",
    "method": "GET",
    "headers": {
      "cache-control": "no-cache",
    }
  };

  $.ajax(settings).done(function (response) {
    obj = JSON.parse(response);
    console.log(obj);
    obj = _.sortBy(obj,'device_id');
    // load hotel
    var content_in_map = [
      'โรงแรมเซ็นทาราและคอนเวนชันเซ็นเตอร์ อุดรธานี',
      'โรงแรมบางกอกพาเลส',
      'ชลจันทร์ พัทยา บีช รีสอร์ท',
      "บริษัท เหรียญไทย อินเตอร์พลาส สาขาบางพลี",
      "บริษัท เหรียญไทย อินเตอร์พลาส จำกัด สาขา เวลโกรว์",
    ];
        // ชลจันทร์ พัทยา บีช รีสอร์ท (Cholchan Pattaya Beach Resort)   location:  12.985936, 100.919362
        // โรงแรมบางกอกพาเลส (Bangkok Palace Hotel)   location:  13.752791, 100.547281
        // โรงแรมเซ็นทาราและคอนเวนชันเซ็นเตอร์ อุดรธานี (Centara Hotel & Convention Centre Udon Thani)  location : 17.407080, 102.800444
    var location_hotel = [
      {lat:  17.407080, lng: 102.800444},
      {lat: 13.752791, lng: 100.547281},
      {lat: 12.985936, lng: 100.919362}
    ];
    // initMap(location_hotel[<?php echo $_SESSION['station_id'];?> - 1]);
    $("#hotel_name").text(content_in_map[ <?php echo $_SESSION['station_id']; ?> - 1]); 

    var content_out = "";
    
    for (var i = 0;i < obj.length;i++){
      content_out += "<div class='box box-primary'>";
      content_out += "<div class='box-header'>";
      content_out += "<span id = '"+obj[i].device_id+"'></span> ";
      content_out += "<h3 class='box-title'><strong>"+obj[i].device_id+" : "+obj[i].device_name+"</strong></h3>";
      content_out += '<div class="box-tools pull-right" id = "date'+obj[i].device_id+'"></div>';
      content_out += "</div>";
      content_out += "<div class='box-body'>";
      content_out += "<div class='container-fluid'>";
      content_out += "<div class='row'>";
      content_out += "<div class='col-md-4'>";
      content_out += "<img src = './images/"+obj[i].img+"'style = 'width: 100%;'/>";
      <?php 
      // for edit image 
      // type user admin
      if ($_SESSION['type'] == "admin"){
      ?>
        // is button
        content_out += "<button style = 'width: 100%; margin-top: 25px;' type='button' class='btn btn-danger' data-toggle='modal' data-target='.bd-example-modal-lg-"+obj[i].device_id+"'>เปลี่ยนรูปภาพ</button>";
        
        // body
        content_out += "<form action='./api/upload_image.php' method='post' enctype='multipart/form-data'><div class='modal fade bd-example-modal-lg-"+obj[i].device_id+"' tabindex='-1' role='dialog' aria-labelledby='myLargeModalLabel' aria-hidden='true'><div class='modal-dialog modal-lg'><div class='modal-content'><div class='modal-header'><button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button><h5 class='modal-title' id='exampleModalLabel' style = 'font-size: 24px;'>เปลี่ยนรูปภาพ "+obj[i].device_id+" : "+obj[i].device_name+"</h5></div><div class='modal-body'><div class='form-group'>"

          content_out += "<label for='upload_img'>Choose Image</label>";
          content_out += "<input type='file' class='form-control-file' name='upload_img' id='upload_img' accept='image/*'/>";
          content_out += "<input hidden id = '' name = 'device_id' value = '"+obj[i].device_id+"'/>";  
          content_out += "<input hidden id = '' name = 'station_id' value = '"+obj[i].station_id+"'/>";        

        content_out += "</div></div><div class='modal-footer'><button type='button' class='btn btn-secondary' data-dismiss='modal'>Close</button><button type='submit' class='btn btn-danger'>Upload</button></div></div></div></div></form>" 


      <?php
      }
      ?>
      content_out += "</div>";
      content_out += "<div class='col-md-8'>";
      content_out += "<table class='table' style = 'font-size: 1.6rem;'>";
      content_out += "<thead><tr style = 'font-: 1.0em;'><th <?php if ($_SESSION['type'] == "user"){echo "hidden";} ?> style='width: 15%'>TAG</th><th <?php if ($_SESSION['type'] == "user"){echo "hidden";} ?> style ='width: 15%;'>TYPE</th><th style='width: 35%;'>SENSOR DESCIPTION</th><th style='width: 10%;'><center>VALUE</center></th><th style='width: 30%;'><center>UNIT</center></th></tr></thead>";
      content_out += "<tbody id = 'body_t"+obj[i].device_id+"'>";
        for (var k = 0;k < obj[i].sensor.length;k++){
          content_out += "<tr style = 'font-size:0.85em;'>";  
            content_out += "<td <?php if ($_SESSION['type'] == "user"){echo "hidden";} ?> id = '' >"+obj[i].sensor[k].tag+"</td>";  
            content_out += "<td <?php if ($_SESSION['type'] == "user"){echo "hidden";} ?> id = '' >"+obj[i].sensor[k].sensor_name+"</td>"; 
            content_out += "<td id = '' >"+obj[i].sensor[k].sensor_desc+"</td>"; 

            // change value and unit
            var unti_ = ""
            var value_ = 0
            if (obj[i].sensor[k].unit == "WH"){
              if (obj[i].sensor[k].val >= 1000000){
                value_ = (obj[i].sensor[k].val / 1000000).toFixed(3)
                unti_ =  "M"
                console.log(value_)
                console.log(unit_)
              }else if (obj[i].sensor[k].val >= 10000){
                value_ = obj[i].sensor[k].val / 1000
                unti_ = "K"
              }
            }else{
              value_ = obj[i].sensor[k].val
              unit_ = ""
            }

            content_out += "<td id = '' style = 'font-size: 1.6em;color: #c5306a;'><center>"+value_+"</center></td>"; 
            content_out += "<td id = '' ><center>" + unti_ + obj[i].sensor[k].unit+"</center></td>"; 
            content_out += "<td>";
            content_out += "</td>"; 
          content_out += "</tr>";
        }
      content_out += "</tbody>";
      content_out += "</table>";
      if (obj[i].sw.length > 0 && user_type != "user"){
        content_out += "<table class='table' style = 'font-size: 1em;'>";
        content_out += "<thead><tr style = 'font-size: 1em;'><th <?php if ($_SESSION['type'] == "user"){echo "hidden";} ?> style='width: 15%;'>TAG</th><th <?php if ($_SESSION['type'] == "user"){echo "hidden";} ?> style = 'width:15%;padding-left: 10px;'>TYPE</th><th style='width: 35%;padding-left: 20px;'>SWITCH DESCIPTION</th><th style='width: 35%;'>CONTROL OUTPUT</th></tr></thead>";
        content_out += "<tbody>";
        for (var s = 0;s < obj[i].sw.length;s++){
          content_out += "<tr style = 'font-size:1em;'>";
          content_out += "<td <?php if ($_SESSION['type'] == "user"){echo "hidden";} ?> style = 'padding: 20px 0px 0px 8px;'>"+obj[i].sw[s].tag+"</td>";
          content_out += "<td <?php if ($_SESSION['type'] == "user"){echo "hidden";} ?> style = 'padding: 20px 0px 0px 10px;'>"+obj[i].sw[s].sensor_name+"</td>";
          content_out += "<td style = 'padding: 20px 0px 0px 20px;'>"+obj[i].sw[s].sensor_desc+"</td>";
          content_out += "<td>";
            content_out += "<div onclick = \"fn_switch_toggle('check"+obj[i].sw[s]._id+"','"+obj[i].sw[s].device_id+"','"+obj[i].sw[s].tag+"');\" class='checkbox'><label><input type='checkbox' "+((obj[i].sw[s].val == 1) ? "checked" : "")+" value = '"+obj[i].sw[s]._id+"' data-toggle='toggle' id = 'check"+obj[i].sw[s]._id+"'></label></div>";
          content_out += "</td>";
          content_out += "</tr>";
        }
          // analog
          if (obj[i].an.length > 0){
            content_out += "<tr style = 'font-size:1em;'>";
            content_out += "<td style = 'padding: 20px 0px 0px 8px;'>"+obj[i].an[0].tag+"</td>";
            content_out += "<td style = 'padding: 20px 0px 0px 10px;'>"+obj[i].an[0].sensor_name+"</td>";
            content_out += "<td style = 'padding: 20px 0px 0px 20px;'>"+obj[i].an[0].sensor_desc+"</td>";
            content_out += "<td>";
            content_out += "<div class='row'>";

              content_out += "<div class='col-sm-4 col-md-4 col-lg-4' style = 'padding-right: 0px;padding-top:10px;'>";
                content_out += "<input value = '"+obj[i].an[0].val+"' id = 'input"+obj[i].an[0].device_id+"'type = 'number' class = 'form-control'/>"
              content_out += "</div>";

              content_out += "<div class='col-sm-4 col-md-4 col-lg-4' style = 'padding-top:10px;'>";
                content_out += "<button class= 'btn btn-primary' onclick = 'analog_change(\""+obj[i].an[0].device_id+"\",\""+obj[i].an[0].tag+"\")'>click</button>";
              content_out += "</div>";

            content_out += "</div>";
            content_out += "</td>";
            content_out += "</tr>";
          }
          // end analog
        content_out += "</tbody>";
        content_out += "</table>";
      }
      content_out += "</div></div></div></div></div>";
    }
    $("#section_box").html("");
    $('#section_box').append(content_out);
    for (var i = 0;i < obj.length;i++){

      // var date = new Date(obj[i].updatedAt);
      
      // var a = moment(date,'YYYY-MM-DD HH:mm:ss');
      // var b = moment(moment().format('YYYY-MM-DD HH:mm:ss'),'YYYY-MM-DD HH:mm:ss');
      // var sum = b.diff(a, "hours");
      // var t = moment(date,'YYYY-MM-DD HH:mm:ss');
      // var time = moment(obj[i].lasttime,'YYYY-MM-DD HH:mm:ss').add(7,"hours").format('YYYY-MM-DD HH:mm:ss');
      // console.log(time);
      var date = moment(obj[i].lasttime,'YYYY-MM-DD HH:mm:ss').add(7,"hours");
      var a = moment(date,'YYYY-MM-DD HH:mm:ss');
      var b = moment(moment().format('YYYY-MM-DD HH:mm:ss'),'YYYY-MM-DD HH:mm:ss');
      var sum = b.diff(a, "hours");
      if (sum < 1){
        $("#" + obj[i].device_id).html("");
        $('#' + obj[i].device_id).append("<i class='fa fa-circle active-color'></i>");
        var t = moment(date).format("DD MMMM YYYY, HH:MM:SS");
        $("#date" + obj[i].device_id).html("");
        $("#date" + obj[i].device_id).append("<span class='box-active'>last active : "+date.format('YYYY-MM-DD HH:mm:ss')+"</span>");
      }else{
        $("#" + obj[i].device_id).html("");
        $('#' + obj[i].device_id).append("<i class='fa fa-circle inactive-color'></i>");
        var t = moment(date).format("DD MMMM YYYY, HH:MM:SS");
        $("#date" + obj[i].device_id).html("");
        $("#date" + obj[i].device_id).append("<span class='box-inactive'>last active : "+date.format('YYYY-MM-DD HH:mm:ss')+"</span>");
      }
    }
    if (user_type != "user"){
      for (var i = 0;i < obj.length;i++){
        if (obj[i].sw.length > 0){
          for (var s = 0;s < obj[i].sw.length;s++){
            // console.log("check"+obj[i].sw[s]._id + " " + obj[i].sw[s].val);
            $('#check'+obj[i].sw[s]._id).bootstrapToggle({on: 'on',off: 'off'});
            if (obj[i].sw[s].val == 1){
              $('#check'+obj[i].sw[s]._id).attr('checked');
            }
          }
        }
      }
    }
  });
}
function fn_switch_toggle(id,device_id,tag){
  console.log(id);
  var comf = confirm("คุณต้องการสั่งการทำงาน Output หรือไม่");
  if (comf == true){
    if (!$("#"+id).prop('checked')){
      console.log("api-control-output : true")
      var settings = {
        "async": true,
        "crossDomain": true,
        "url": "http://209.97.163.214:4001/mqtt",
        "method": "POST",
        "headers": {
          "Content-Type": "application/json",
          "Accept": "*/*",
          "Cache-Control": "no-cache",
          "cache-control": "no-cache"
        },
        "processData": false,
        "data": "{\n\t\"device_id\":\""+device_id+"\",\n\t\"tag\":\""+tag+"\",\n\t\"val\":1\n}"
      }

      $.ajax(settings).done(function (response) {
        console.log(response);
      });
    }else{
      console.log("api-control-output : false")
      var settings = {
        "async": true,
        "crossDomain": true,
        "url": "http://209.97.163.214:4001/mqtt",
        "method": "POST",
        "headers": {
          "Content-Type": "application/json",
          "Accept": "*/*",
          "Cache-Control": "no-cache"
        },
        "processData": false,
        "data": "{\n\t\"device_id\":\""+device_id+"\",\n\t\"tag\":\""+tag+"\",\n\t\"val\":0\n}"
      }

      $.ajax(settings).done(function (response) {
        console.log(response);
      });
    }
  }else{
    if($("#"+id).prop('checked')){
      ch = 'off';
    }else{
      ch = 'on';
    }
    console.log(ch);
    $("#"+id).bootstrapToggle(ch)
  }
}


function draw_active_color() {
    var mrefreshinterval = 100; 
    var lastmousetime;
    var mousetravel = 0;
    var mdraw = function () {
      var md = new Date();
      var timenow = md.getTime();
      if (lastmousetime && lastmousetime != timenow) {

      }
      lastmousetime = timenow;
      setTimeout(mdraw, mrefreshinterval);
    };
    setTimeout(mdraw, mrefreshinterval);
  }
  function analog_change(device_ids,tags){
    var val = $("#input"+device_ids).val();
    var ch = confirm("คุณต้องการเปลี่ยนค่า Analog ไหม");
    if (val != "" && ch == true){
      var settings = {
        "async": true,
        "crossDomain": true,
        "url": "http://209.97.163.214:4001/mqtt",
        "method": "POST",
        "headers": {
          "Content-Type": "application/json",
          "cache-control": "no-cache",
          "Accept": "*/*",
        },
        "processData": false,
        "data": "{ \n\t\"device_id\":\""+device_ids+"\",\n\t\"tag\":\""+tags+"\",\n\t\"val\":"+val+"\n}"
      }

      $.ajax(settings).done(function (response) {
        console.log(response);
      });
    }
  }
  function formatNumber(num) {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
  }
</script>
</body>
</html>
