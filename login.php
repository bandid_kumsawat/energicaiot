<?php 
  @session_start();
  @session_destroy();
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Energica Login</title>
    <link rel="stylesheet" href="./dist/css/style.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans|Roboto:100,400,900" rel="stylesheet">
  </head>
  <body>
  <div class="fullscreen-block"></div>
    <!-- Main container -->
    <main>
      <!-- Form title Login/Sign UP -->
      <header>
        <h1 class="form-title"><img src="./images/energica.png" width="250" height="114"></h1>
      </header>
      <!-- <form action="./login.php" method="post" value = "true" name = 'login'> -->
        <input type="text" id = "username" name="username" value="" placeholder="Username">
        <input type="password" id = "password" name="password" value="" placeholder="Password">
        <button type="submit" name="submit" value="submit" onclick = "on_click()">Login</button>
      <!-- </form> -->
  </main>

  <!-- jQuery 3 -->
  <script src="bower_components/jquery/dist/jquery.min.js"></script>
  <script language="JavaScript" >
    function on_click(){
      if ($("#username").val() != "" && $("#password").val() != ""){
        var settings = {
          "async": true,
          "crossDomain": true,
          "url": "http://www.energicaiot.com:4000/api/users/login",
          "method": "POST",
          "headers": {
            "Content-Type": "application/json",
            "X-Requested-With": "XMLHttpRequest",
            "Accept": "*/*",
            "Cache-Control": "no-cache",
            "cache-control": "no-cache"
          },
          "processData": false,
          "data": "{\"user\":{\"username\":\""+$("#username").val()+"\", \"password\":\""+$("#password").val()+"\"}}"
        }

        $.ajax(settings).done(function (data) {
          try {
            console.log(data.status);
            if (data.status){
              if (data.user.type == "admin"){
                location.replace("./create_session.php?token=" + data.user.token + "&username=" +data.user.username + "&email=" + data.user.email + "&type=" + data.user.type + "&station_id=1");
              }else{
                location.replace("./create_session.php?token=" + data.user.token + "&username=" +data.user.username + "&email=" + data.user.email + "&type=" + data.user.type + "&station_id=" + data.user.station_id[0]);
              }
            }else{
              alert ('ไม่สามารถล๊อกอิน');
              // location.replace("./login.php");
            }
            // location.replace("./create_session.php?token=" + data.user.token + "&username=" +data.user.username + "&email=" + data.user.email + "&type=" + data.user.type + "&station_id=" + data.user.station_id[0]);
          } catch {
            alert ('เซิฟเวอร์มีปัญหา');
            location.replace("./login.php");
          }
        });     
      }else{
        alert("กรุณากรอกข้อมูลให้ครบถ้วน");
      }
    }
  </script>
</body>
</html>
